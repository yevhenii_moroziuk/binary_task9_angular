﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Library.BusinessLogic.DTOs;
using Library.Services;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
	[Route("api/tasks")]
	[ApiController]
	public class TaskController : ControllerBase
	{
		private readonly ICrudService<TaskDTO> _service;

		public TaskController(ICrudService<TaskDTO> service)
		{
			_service = service;
		}

		[HttpGet]
		public async Task<ActionResult<List<TaskDTO>>> GetTasksAsync()
		{
			return await _service.ShowAllAsync();
		}

		[HttpGet("{id}")]
		public async Task<IActionResult> GetTaskAsync(int id)
		{
			if (!await _service.IsExistsAsync(id))
				return NotFound();

			return Ok(await _service.ShowAsync(id));
		}

		[HttpPost]
		public async Task<IActionResult> PostTaskAsync(TaskDTO taskDTO)
		{
			if (!ModelState.IsValid)
				return BadRequest();

			if (await _service.IsExistsAsync(taskDTO.Id))
				return BadRequest();

			try
			{
				await _service.CreateAsync(taskDTO);
				return Created("api/tasks", taskDTO);
			}
			catch
			{
				//throw new Exception("Invalid team Id");
				return BadRequest("Invalid data");
			}
		}

		[HttpPut]
		public async Task<IActionResult> EditTaskAsync(TaskDTO entity)
		{
			if (!await _service.IsExistsAsync(entity.Id))
				return NotFound();

			try
			{
				await _service.UpdateAsync(entity);
				return Ok();
			}
			catch
			{
				return BadRequest("Invalid data");
			}
		}

		[HttpDelete("{id}")]
		public async Task<IActionResult> DeleteTasksAsync(int id)
		{
			if (!await _service.IsExistsAsync(id))
				return BadRequest();

			await _service.DeleteAsync(id);

			return Ok();
		}
	}
}