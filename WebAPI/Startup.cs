﻿using AutoMapper;
using Library.BusinessLogic.DTOs;
using Library.DataAccess;
using Library.DataAccess.Seed;
using Library.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;
using WebAPI.Hubs;
using WebAPI.Queue;

namespace WebAPI
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddDbContext<BinaryDataContext>(options =>
			{
				options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"),
					x => x.MigrationsAssembly("Library"));
			});
			services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2).AddJsonOptions(options =>
            {
                var resolver = options.SerializerSettings.ContractResolver;
                if (resolver != null)
                    (resolver as DefaultContractResolver).NamingStrategy = null;
            });

            services.AddAutoMapper();
			services.AddTransient<SeedService>();

    		services.AddSignalR();
			services.AddScoped<QueueService>();
			services.AddCors();

			services.AddScoped<ICrudService<UserDTO>, UserService>();
			services.AddScoped<ICrudService<TeamDTO>, TeamService>();
			services.AddScoped<ICrudService<TaskDTO>, TaskService>();
			services.AddScoped<ICrudService<ProjectDTO>, ProjectService>();
			services.AddScoped<ILinqTaskService, LinqTaskService>();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
				app.UseHsts();
			}
			//seed.SeedEntities();
			app.UseCors(c => c.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
			app.UseSignalR(routes =>
			{
				routes.MapHub<MessageHub>("/hubmes");
			});
			app.UseHttpsRedirection();
			app.UseMvc();
		}
	}
}
