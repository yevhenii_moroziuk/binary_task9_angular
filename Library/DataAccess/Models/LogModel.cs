﻿using System;

namespace Library.DataAccess.Models
{
	public class LogModel
	{
		public DateTime WrittenData { get; set; }
		public string Message { get; set; }
	}
}
