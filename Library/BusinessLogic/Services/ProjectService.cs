﻿using AutoMapper;
using Library.BusinessLogic.DTOs;
using Library.DataAccess;
using Library.DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace Library.Services
{
	public class ProjectService : ICrudService<ProjectDTO>
	{
		private readonly BinaryDataContext _context;
		private readonly IMapper _mapper;

		public ProjectService(BinaryDataContext source, IMapper mapper)
		{
			_context = source;
			_mapper = mapper;
		}

		public async Task CreateAsync(ProjectDTO entity)
		{
			//Project has User & Team FK
			if (_context.Users.SingleOrDefault(u => u.Id == entity.Author_Id) == null ||
				_context.Teams.SingleOrDefault(t => t.Id == entity.Team_Id) == null)
				throw new Exception("Invalid AuthorId or TeamId");


			await _context.Projects.AddAsync(_mapper.Map<Project>(entity));
			await _context.SaveChangesAsync();
		}

		public async Task DeleteAsync(int id)
		{
			var projects = await _context.Projects.SingleOrDefaultAsync(u => u.Id == id);
			_context.Projects.Remove(projects);

			await _context.SaveChangesAsync();
		}

		public async Task<ProjectDTO> ShowAsync(int Id)
		{
			var project = await _context.Projects.SingleOrDefaultAsync(u => u.Id == Id);
			return  _mapper.Map<ProjectDTO>(project);
		}

		public async Task<List<ProjectDTO>> ShowAllAsync()
		{
			var projects = await _context.Projects.ToListAsync();
			return _mapper.Map<List<ProjectDTO>>(projects);
		}

		public async Task UpdateAsync(ProjectDTO entity)
		{
			//Project has User & Team FK
			if (_context.Users.SingleOrDefault(u => u.Id == entity.Author_Id) == null ||
				_context.Teams.SingleOrDefault(t => t.Id == entity.Team_Id) == null)
				throw new Exception("Invalid AuthorId or TeamId");


			var projects = await _context.Projects.SingleOrDefaultAsync(u => u.Id == entity.Id);//find this user in collection
			var tprojects = _mapper.Map<Project>(entity);

            projects.Id = tprojects.Id;
            projects.Name = tprojects.Name;
            projects.Description = tprojects.Description;
            projects.Author_Id = tprojects.Author_Id;
            projects.Created_At = tprojects.Created_At;
            projects.Team_Id = tprojects.Team_Id;

			await _context.SaveChangesAsync();
		}

		public async Task<bool> IsExistsAsync(int id)
		{
			return await _context.Projects.SingleOrDefaultAsync(u => u.Id == id) == null ? false : true;
		}
	}
}