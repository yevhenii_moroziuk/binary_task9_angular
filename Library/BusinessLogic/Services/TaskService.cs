﻿using AutoMapper;
using Library.BusinessLogic.DTOs;
using Library.DataAccess;
using Library.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Library.Services
{
	public class TaskService : ICrudService<TaskDTO>
	{
		private readonly BinaryDataContext _context;
		private readonly IMapper _mapper;

		public TaskService(BinaryDataContext source, IMapper mapper)
		{
			_context = source;
			_mapper = mapper;
		}

		public async Task CreateAsync(TaskDTO entity)
		{
			//Task has Project & Team FK
			if (_context.Users.SingleOrDefault(u => u.Id == entity.Performer_Id) == null ||
				_context.Projects.SingleOrDefault(p => p.Id == entity.Project_Id) == null)
				throw new Exception("Invalid PerformerId or ProjectID");

			await _context.Tasks.AddAsync(_mapper.Map<DataAccess.Models.Task>(entity));

			await _context.SaveChangesAsync();
		}

		public async Task DeleteAsync(int id)
		{
			var task = await _context.Tasks.SingleOrDefaultAsync(u => u.Id == id);
			_context.Tasks.Remove(task);

			await _context.SaveChangesAsync();
		}

		public async Task<TaskDTO> ShowAsync(int Id)
		{
			var task = await _context.Tasks.SingleOrDefaultAsync(u => u.Id == Id);
			return _mapper.Map<TaskDTO>(task);
		}

		public async Task<List<TaskDTO>> ShowAllAsync()
		{
			var tasks = await _context.Tasks.ToListAsync();
			return _mapper.Map<List<TaskDTO>>(tasks);
		}

		public async Task UpdateAsync(TaskDTO entity)
		{
			//Task has Project & Team FK
			if (_context.Users.SingleOrDefault(u => u.Id == entity.Performer_Id) == null ||
				_context.Projects.SingleOrDefault(p => p.Id == entity.Project_Id) == null)
				throw new Exception("Invalid PerformerId or ProjectID");

			var task = await _context.Tasks.SingleOrDefaultAsync(u => u.Id == entity.Id);//find this user in collection
			var ttask = _mapper.Map<DataAccess.Models.Task>(entity);

            task.Id = ttask.Id;
            task.Name = ttask.Name;
            task.Project_Id = ttask.Project_Id;
            task.Performer_Id = ttask.Performer_Id;
            task.State = ttask.State;
            task.Description = ttask.Description;
            task.Created_At = ttask.Created_At;
            task.Finished_At = ttask.Finished_At;

            //_context.Tasks.Update(task);
			await _context.SaveChangesAsync();
		}

		public async Task<bool> IsExistsAsync(int id)
		{
			return await _context.Tasks.SingleOrDefaultAsync(u => u.Id == id) == null ? false : true;
		}
	}
}