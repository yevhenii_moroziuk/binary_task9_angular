﻿using Library.BusinessLogic.DTOs;
using Library.DataAccess.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Library.Services
{
	public interface ILinqTaskService
	{
		Task<IDictionary<string, int>> GetTasksCountAsync(int author);
		Task<List<DataAccess.Models.Task>> GetTasksAsync(int user_Id);
		Task<List<string>> GetFinishedTasksAsync(int user_Id);
		Task<IDictionary<string, List<User>>> GetTeamDefinitionAsync();
		Task<IDictionary<User, List<DataAccess.Models.Task>>> GetUserTasksAsync();
		Task<Task6DTO> GetUserDefinitionAsync(int user_id);
		Task<Task7DTO> GetProjectDefinitionAsync(int project_id);
		Task<List<LogModel>> GetLogAsync();
	}
}
