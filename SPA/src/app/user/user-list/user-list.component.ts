import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { User } from '../user';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  constructor(private _service:UserService, private _alertify:AlertifyService) { }

  EditUser(user:User){
    this._service.userFormData = Object.assign({},user);
    this._alertify.warning('selected');
  }

  DeleteUser(id:number){

    this._service.deleteUser(id).subscribe(next=>{
      this._service.getUsers();
      this._alertify.success('deleted');
    }, error =>{
      this._alertify.error('can`t delete item');
    })
  }

  ngOnInit() {
    this._service.getUsers();
  }

}