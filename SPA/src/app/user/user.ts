export class User {
    Id: number;
    First_Name: string;
    Last_Name: string;
    Birthday: Date;
    Registered_At: Date;
    Email: number;
    Team_Id?: number;
}
