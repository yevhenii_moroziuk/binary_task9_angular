import { Injectable } from '@angular/core';
import { User } from '../user';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService{

  userFormData: User = new User();
  users:User[];
  address: string = environment.address + '/users';

  constructor(private _client: HttpClient) { }

  getUsers() {/*
    return this._client.get(this.address)
    .toPromise()
    .then(res => this.tasks = res as Task[]);*/
    return this._client.get(this.address).pipe(map((data:User[])=>{
      this.users = data;
    })).subscribe(next => {
    }, error=>{
      console.log(error);
    });

  }

  editUser(){
    console.log(this.userFormData);
    return this._client.put(this.address, this.userFormData);
  }

  deleteUser(id:number){
    return this._client.delete(this.address+"/"+id);
  }

  postUser() {
    console.log(this.userFormData);
    return this._client.post(this.address, this.userFormData);
  }
}