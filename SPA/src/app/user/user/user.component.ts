import { Component, OnInit, OnDestroy } from '@angular/core';
import { LeavePageGuard } from 'src/app/_guard/leave-page.guard';
import { UserService } from '../services/user.service';
import { User } from '../user';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent extends LeavePageGuard implements OnInit, OnDestroy {
  
  ngOnDestroy(): void {
    this._service.userFormData = new User();
  }

  constructor(private _service:UserService) {
    super();
  }

  canDeactivate():boolean{
    return this._service.userFormData.Birthday ==null && 
           this._service.userFormData.First_Name == null &&
           this._service.userFormData.Email==null &&
           this._service.userFormData.Id == null &&
           this._service.userFormData.Registered_At == null &&
           this._service.userFormData.Last_Name == null &&
           this._service.userFormData.Team_Id == null;
  }

  ngOnInit() {
  }

}
