import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './user/user.component';
import { UserFormComponent } from './user-form/user-form.component';
import { UserService } from './services/user.service';
import { UserListComponent } from './user-list/user-list.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../-shared/-shared.module';

@NgModule({
  declarations: [UserComponent, UserFormComponent,UserListComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,SharedModule
  ],
  providers: [UserService]
})
export class UserModule { }

