import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { NgForm } from '@angular/forms';
import { User } from '../user';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

  constructor(private _service:UserService, private _alertify:AlertifyService) { }

  addUser(form:NgForm){
    this._service.postUser().subscribe(res=>{

      this._service.getUsers();
      this._service.userFormData = new User();
      form.resetForm();

      this._alertify.success('created')
      }, error =>{
        this._alertify.error('an error occured while added item');
    });
  }

  editUser(form:NgForm){
    this._service.editUser().subscribe(res=>{

      this._service.getUsers();
      this._service.userFormData = new User();
      form.resetForm();

      this._alertify.success('edited')
      }, error =>{
        this._alertify.error('an error occured while updated item');
      });
  }

  ngOnInit() {
  }

}
