import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { LeavePageGuard } from 'src/app/_guard/leave-page.guard';
import { ProjectService } from '../services/project.service';
import { Project } from '../project';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent extends LeavePageGuard implements OnInit, OnDestroy  {
  
  ngOnDestroy(): void {
    this._service.projectFormData = new Project();
  }
  
  constructor(private _service:ProjectService) {
    super();
  }

  canDeactivate():boolean{
    return this._service.projectFormData.Author_Id==null && 
           this._service.projectFormData.Created_At==null &&
           this._service.projectFormData.Name == null &&
           this._service.projectFormData.Description==null &&
           this._service.projectFormData.Id == null &&
           this._service.projectFormData.Team_Id == null;
  }

  ngOnInit() {
  }

}
