import { Injectable } from '@angular/core';
import { Project } from '../project';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class ProjectService {

  projectFormData: Project = new Project();
  projects:Project[];
  address: string = environment.address + '/projects';

  constructor(private _client: HttpClient) { }

  getProjects() {
    return this._client.get(this.address).pipe(map((data:Project[])=>{
      this.projects = data;
    })).subscribe(next => {
    }, error=>{
      console.log(error);
    });

  }

  editProject(){
    console.log(this.projectFormData);
    return this._client.put(this.address, this.projectFormData);
  }

  deleteProject(id:number){
    return this._client.delete(this.address+"/"+id);
  }

  postProject() {
    console.log(this.projectFormData);
    return this._client.post(this.address, this.projectFormData);
  }
}
