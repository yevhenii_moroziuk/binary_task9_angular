import { Component, OnInit } from '@angular/core';
import { ProjectService } from '../services/project.service';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { Project } from '../project';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css']
})
export class ProjectListComponent implements OnInit {

  constructor(private _service:ProjectService, private _alertify:AlertifyService) { }

  EditProject(project:Project){
    this._service.projectFormData = Object.assign({},project);
    this._alertify.warning('selected');
  }

  DeleteProject(id:number){

    this._service.deleteProject(id).subscribe(next=>{
      this._service.getProjects();
      this._alertify.success('deleted');
    }, error =>{
      this._alertify.error('can`t delete item');
    })
  }

  ngOnInit() {
    this._service.getProjects();
  }

}
