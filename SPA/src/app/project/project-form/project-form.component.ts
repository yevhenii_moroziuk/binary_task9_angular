import { Component, OnInit, ViewChild } from '@angular/core';
import { ProjectService } from '../services/project.service';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { NgForm } from '@angular/forms';
import { Project } from '../project';

@Component({
  selector: 'app-project-form',
  templateUrl: './project-form.component.html',
  styleUrls: ['./project-form.component.css']
})
export class ProjectFormComponent implements OnInit {

  constructor(private _service:ProjectService, private _alertify:AlertifyService) { }

  addProject(form:NgForm){
    this._service.postProject().subscribe(res=>{

      this._service.getProjects();
      this._service.projectFormData = new Project();
      form.resetForm();

      this._alertify.success('created')
      }, error =>{
        this._alertify.error('an error occured while added item');
    });
  }

  editProject(form:NgForm){
    this._service.editProject().subscribe(res=>{

      this._service.getProjects();
      this._service.projectFormData = new Project();
      form.resetForm();

      this._alertify.success('edited')
      }, error =>{
        this._alertify.error('an error occured while updated item');
      });
  }

  ngOnInit() {
  }

}
