export class Project {
    Id: number;
    Name: string;
    Description: string;
    Created_At: Date;
    Author_Id?: number;
    Team_Id?: number;
}
