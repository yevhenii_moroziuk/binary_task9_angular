import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectComponent } from './project/project.component';
import { ProjectFormComponent } from './project-form/project-form.component';
import { ProjectListComponent } from './project-list/project-list.component';
import { SharedModule } from '../-shared/-shared.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ProjectService } from './services/project.service';

@NgModule({
  declarations: [ProjectComponent, ProjectFormComponent, ProjectListComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    SharedModule
    ],
  providers:[ProjectService]
})
export class ProjectModule { }
