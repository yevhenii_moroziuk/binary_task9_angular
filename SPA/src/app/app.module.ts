import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TaskModule } from './task/task.module';
import { HomeComponent } from './home/home.component';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/uk';
import { AlertifyService } from './_services/alertify.service';
import { UserModule } from './user/user.module';
import { TeamModule } from './team/team.module';
import { ProjectModule } from './project/project.module';
import { LeavePageGuard } from './_guard/leave-page.guard';


registerLocaleData(localeFr);

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
            ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    TaskModule,
    UserModule,
    TeamModule,
    ProjectModule
  ],    
  providers: [AlertifyService, LeavePageGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
