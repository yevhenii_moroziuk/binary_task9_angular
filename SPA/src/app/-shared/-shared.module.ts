import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DateUaPipe } from '../_pipes/dateUa.pipe';
import { ShortDePipe } from '../_pipes/short-de.pipe';

@NgModule({
  declarations: [DateUaPipe, ShortDePipe],
  imports: [
    CommonModule
  ],
   exports:[
    DateUaPipe,
    ShortDePipe
]
})
export class SharedModule { }
