import { Directive, Input, ElementRef, OnInit } from '@angular/core';

@Directive({
  selector: '[appExample]'
})
export class TaskDirectiveDirective implements OnInit{
 
  @Input() taskStatus: number;

  constructor(private elementRef: ElementRef)  {
    
    //this.highlight(this.taskStatus, this.elementRef);
   // this.elementRef.nativeElement.className = 'badge badge-pill badge-primary';
   // this.elementRef.nativeElement.innerHTML = 'work';
    console.log(this.elementRef);
    console.log(this.taskStatus);
  }

  ngOnInit(): void {
    this.highlight(this.taskStatus, this.elementRef);
    console.log(this.elementRef);
    console.log(this.taskStatus);
  }
  

  //Created = 0,
	//Started = 1,
	//Finished = 2,
  //Canceled = 3
  private highlight(taskStatus: number, el:ElementRef) {
    switch (taskStatus) {
      case 0:
        this.elementRef.nativeElement.className = 'badge badge-pill badge-info';
        this.elementRef.nativeElement.innerHTML = 'Created';
        break;
      case 1: 
        this.elementRef.nativeElement.className = 'badge badge-pill badge-primary';
        this.elementRef.nativeElement.innerHTML = 'Started';
        break;
      case 2:
        this.elementRef.nativeElement.className = 'badge badge-pill badge-success';
        this.elementRef.nativeElement.innerHTML = 'Finished'; 
        break;
      case 3:    
        this.elementRef.nativeElement.className = 'badge badge-pill badge-secondary';
        this.elementRef.nativeElement.innerHTML = 'Canceled';
        break;
    }

  }
}
