import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TaskComponent } from './task/task/task.component';
import { HomeComponent } from './home/home.component';
import { UserComponent } from './user/user/user.component';
import { TeamComponent } from './team/team/team.component';
import { ProjectComponent } from './project/project/project.component';
import { LeavePageGuard } from './_guard/leave-page.guard';

const routes: Routes = [
  { path: 'tasks', component: TaskComponent, canDeactivate: [LeavePageGuard] },
  { path: 'users', component: UserComponent, canDeactivate: [LeavePageGuard] },
  { path: 'teams', component: TeamComponent, canDeactivate: [LeavePageGuard] },
  { path: 'projects', component: ProjectComponent, canDeactivate: [LeavePageGuard]  },
  {path: '**', component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
