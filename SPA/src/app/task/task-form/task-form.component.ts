import { Component, OnInit } from '@angular/core';
import { TaskService } from '../services/task.service';
import { NgForm } from '@angular/forms';
import { Task } from '../task';
import { AlertifyService } from 'src/app/_services/alertify.service';

@Component({
  selector: 'app-task-form',
  templateUrl: './task-form.component.html',
  styleUrls: ['./task-form.component.css']
})
export class TaskFormComponent implements OnInit {

  constructor(private _service:TaskService, private _alertify:AlertifyService) { }

  addTask(form:NgForm){
    this._service.postTask().subscribe(res=>{

      this._service.getTasks();
      this._service.taskFormData = new Task();
      form.resetForm();

      this._alertify.success('created')
      }, error =>{
        this._alertify.error('an error occured while added item');
    });
  }

  editTask(form:NgForm){
    this._service.editTask().subscribe(res=>{

      this._service.getTasks();
      this._service.taskFormData = new Task();
      form.resetForm();

      this._alertify.success('edited')
      }, error =>{
        this._alertify.error('an error occured while updated item');
      });
  }

  ngOnInit() {
  }

}
