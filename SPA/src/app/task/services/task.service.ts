import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Task } from '../task';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  taskFormData: Task = new Task();
  tasks:Task[];
  address: string = environment.address + '/tasks';

  constructor(private _client: HttpClient) { }

  getTasks() {/*
    return this._client.get(this.address)
    .toPromise()
    .then(res => this.tasks = res as Task[]);*/
    return this._client.get(this.address).pipe(map((data:Task[])=>{
      this.tasks = data;
    })).subscribe(next => {
    }, error=>{
      console.log(error);
    });

  }

  editTask(){
    console.log(this.taskFormData);
    return this._client.put(this.address, this.taskFormData);
  }

  deleteTask(id:number){
    return this._client.delete(this.address+"/"+id);
  }

  postTask() {
    console.log(this.taskFormData);
    return this._client.post(this.address, this.taskFormData);
  }
}
