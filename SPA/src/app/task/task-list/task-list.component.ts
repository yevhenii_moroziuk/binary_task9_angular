import { Component, OnInit } from '@angular/core';
import { TaskService } from '../services/task.service';
import { Task } from '../task';
import { tap, map } from "rxjs/operators";
import { AlertifyService } from 'src/app/_services/alertify.service';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit {

  constructor(private _service:TaskService, private _alertify:AlertifyService) { }

  EditTask(task:Task){
    this._service.taskFormData = Object.assign({},task);
    this._alertify.warning('selected');
  }

  DeleteTask(id:number){

    this._service.deleteTask(id).subscribe(next=>{
      this._service.getTasks();
      this._alertify.success('deleted');
    }, error =>{
      this._alertify.error('can`t delete item');
    })
  }

  ngOnInit() {
    this._service.getTasks();
  }

}
