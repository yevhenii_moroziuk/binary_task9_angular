import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaskFormComponent } from './task-form/task-form.component';
import { TaskListComponent } from './task-list/task-list.component';
import { TaskComponent } from './task/task.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from "@angular/forms";
import { TaskService } from './services/task.service';
import { SharedModule } from '../-shared/-shared.module';
import { TaskDirectiveDirective } from '../_directive/task-directive.directive';


@NgModule({
  declarations: [TaskFormComponent, TaskListComponent, TaskComponent, TaskDirectiveDirective],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    SharedModule,

    ],
  providers:[TaskService]
})
export class TaskModule { }
