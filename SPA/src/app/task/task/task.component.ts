import { Component, OnInit, OnDestroy } from '@angular/core';
import { Task } from '../task';
import { TaskService } from '../services/task.service';
import { LeavePageGuard } from 'src/app/_guard/leave-page.guard';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent extends LeavePageGuard implements OnInit, OnDestroy{
  
  ngOnDestroy(): void {
    this._service.taskFormData = new Task();
  }

  constructor(private _service:TaskService) {
    super();
  }

  canDeactivate():boolean{
    return this._service.taskFormData.Created_At ==null && 
           this._service.taskFormData.Description==null &&
           this._service.taskFormData.Name == null &&
           this._service.taskFormData.Finished_At==null &&
           this._service.taskFormData.Id == null &&
           this._service.taskFormData.State == null &&
           this._service.taskFormData.Project_Id == null &&
           this._service.taskFormData.Performer_Id == null;
  }

  ngOnInit() {
  }

}
