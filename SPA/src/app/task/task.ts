export class Task {
    Id: number;
    Name: string;
    Description: string;
    Created_At: Date;
    Finished_At: Date;
    State: TaskState;
    Project_Id?: number;
    Performer_Id?: number;
}

export enum TaskState{
    Created = 0,
	Started = 1,
	Finished = 2,
	Canceled = 3
}