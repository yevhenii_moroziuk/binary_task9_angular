import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'shortDe'
})
export class ShortDePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return value.substr(0,25)+'...';
  }

}
