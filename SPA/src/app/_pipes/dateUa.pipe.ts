import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({
  name: 'dateUa'
})

export class DateUaPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let datePipe = new DatePipe("uk")
    value = datePipe.transform(value, 'd MMMM y')
    return value;
  }

}
