
import {HostListener} from "@angular/core";
import { NgForm } from '@angular/forms';

export abstract class ComponentCanDeactivate {
 
  abstract get form():NgForm;
 
  canDeactivate():boolean{
       return this.form.submitted || !this.form.dirty
   }
}