import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeamComponent } from './team/team.component';
import { TeamListComponent } from './team-list/team-list.component';
import { TeamService } from './services/team.service';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from '../-shared/-shared.module';
import { FormsModule } from '@angular/forms';
import { TeamFormComponent } from './team-form/team-form.component';

@NgModule({
  declarations: [TeamComponent, TeamFormComponent,TeamListComponent], 
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    SharedModule
  ],
  providers: [TeamService]
})
export class TeamModule { }
