import { Component, OnInit, OnDestroy } from '@angular/core';
import { LeavePageGuard } from 'src/app/_guard/leave-page.guard';
import { TeamService } from '../services/team.service';
import { Team } from '../team';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']
})
export class TeamComponent extends LeavePageGuard implements OnInit, OnDestroy {
  
  ngOnDestroy(): void {
    this._service.teamFormData = new Team();
  }

  constructor(private _service:TeamService) {
    super();
  }

  canDeactivate():boolean{
    return this._service.teamFormData.Created_At ==null && 
           this._service.teamFormData.Name == null &&
           this._service.teamFormData.Id == null;
  }

  ngOnInit() {
  }

}
