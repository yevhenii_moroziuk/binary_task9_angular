import { Component, OnInit } from '@angular/core';
import { TeamService } from '../services/team.service';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { NgForm } from '@angular/forms';
import { Team } from '../team';

@Component({
  selector: 'app-team-form',
  templateUrl: './team-form.component.html',
  styleUrls: ['./team-form.component.css']
})
export class TeamFormComponent implements OnInit {

  constructor(private _service:TeamService, private _alertify:AlertifyService) { }

  addTeam(form:NgForm){
    this._service.postTeam().subscribe(res=>{

      this._service.getTeams();
      this._service.teamFormData = new Team();
      form.resetForm();

      this._alertify.success('created')
      }, error =>{
        this._alertify.error('an error occured while added item');
    });
  }

  editTeam(form:NgForm){
    this._service.editTeam().subscribe(res=>{

      this._service.getTeams();
      this._service.teamFormData = new Team();
      form.resetForm();

      this._alertify.success('edited')
      }, error =>{
        this._alertify.error('an error occured while updated item');
      });
  }

  ngOnInit() {
  }

}
