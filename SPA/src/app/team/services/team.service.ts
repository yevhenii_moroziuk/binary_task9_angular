import { Injectable } from '@angular/core';
import { Team } from '../team';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TeamService {

  teamFormData: Team = new Team();
  teams:Team[];
  address: string = environment.address + '/teams';

  constructor(private _client: HttpClient) { }

  getTeams() {
    return this._client.get(this.address).pipe(map((data:Team[])=>{
      this.teams = data;
    })).subscribe(next => {
    }, error=>{
      console.log(error);
    });

  }

  editTeam(){
    console.log(this.teamFormData);
    return this._client.put(this.address, this.teamFormData);
  }

  deleteTeam(id:number){
    return this._client.delete(this.address+"/"+id);
  }

  postTeam() {
    console.log(this.teamFormData);
    return this._client.post(this.address, this.teamFormData);
  }
}
