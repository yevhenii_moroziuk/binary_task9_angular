import { Component, OnInit } from '@angular/core';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { TeamService } from '../services/team.service';
import { Team } from '../team';

@Component({
  selector: 'app-team-list',
  templateUrl: './team-list.component.html',
  styleUrls: ['./team-list.component.css']
})
export class TeamListComponent implements OnInit {

  constructor(private _service:TeamService, private _alertify:AlertifyService) { }

  EditTeam(team:Team){
    this._service.teamFormData = Object.assign({},team);
    this._alertify.warning('selected');
  }

  DeleteTeam(id:number){

    this._service.deleteTeam(id).subscribe(next=>{
      this._service.getTeams();
      this._alertify.success('deleted');
    }, error =>{
      this._alertify.error('can`t delete item');
    })
  }

  ngOnInit() {
    this._service.getTeams();
  }

}
